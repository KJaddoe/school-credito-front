(function() { 'use strict';

    var gulp = require('gulp');
    var rename = require('gulp-rename');
    var plumber = require('gulp-plumber');
    var concat = require('gulp-concat');
    var jshint = require('gulp-jshint');
    var uglify = require('gulp-uglify');
    var less = require('gulp-less');
    var cssmin = require('gulp-cssmin');
    var pug = require('gulp-pug');
    var image = require('gulp-image');

    var BUILD_FOLDER = './dist/';

    gulp.task('watch', function() {

        gulp.watch('./app/**/*.js', ['js']);
        gulp.watch('./app/**/*.less', ['css']);
        gulp.watch('./app/**/*.pug', ['html']);
        gulp.watch('./app/media/*', ['images']);

    });

    gulp.task('js', function() {

        gulp.src(['app/**/*.module.js', 'app/**/*.config.js', 'app/**/*.js'])
            .pipe(plumber())
            .pipe(jshint())
            .pipe(jshint.reporter('jshint-stylish'))
            .pipe(jshint.reporter('fail'))
            .pipe(concat('app.js'))
            .pipe(gulp.dest(BUILD_FOLDER + 'js/'))
            .pipe(uglify())
            .pipe(rename({ extname: '.min.js' }))
            .pipe(gulp.dest(BUILD_FOLDER + 'js/'))
            .on('error', function(err) {
                console.log(err);
                this.emit('end');
            });

    });

    gulp.task('css', function() {

        gulp.src(['./app/index.less'])
            .pipe(plumber())
            .pipe(less())
            .pipe(rename({ basename: 'app' }))
            .pipe(gulp.dest(BUILD_FOLDER + 'css/'))
            .pipe(cssmin())
            .pipe(rename({ extname: '.min.css' }))
            .pipe(gulp.dest(BUILD_FOLDER + 'css/'))
            .on('error', function(err) {
                console.log(err);
                this.emit('end');
            });

    });

    gulp.task('html', function() {

        gulp.src(['./app/**/*.pug'])
            .pipe(plumber())
            .pipe(pug())
            .pipe(rename({ dirname: '', extname: '.html' }))
            .pipe(gulp.dest(BUILD_FOLDER + 'html/'))
            .on('error', function(err) {
                console.log(err);
                this.emit('end');
            });

    });

    gulp.task('images', function() {

        gulp.src(['./app/media/**'])
            .pipe(image())
            .pipe(gulp.dest(BUILD_FOLDER + 'media/'));

    });

    gulp.task('default', ['js', 'css', 'html', 'images','watch']);

})();
