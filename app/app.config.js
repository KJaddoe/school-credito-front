(function() { 'use strict';

    angular
        .module('app')
        .factory('config', config);

    config.$inject = ['$log'];

    function config($log) {

        return {
            init: init,
            get: get,
            set: set,
        };

        function init(localConfig) {

            /* jshint validthis:true */
            var _this = this;

            localConfig(_this);

            _this.set('baseUrl', (_this.get('protocol') + _this.get('apiHost')));


        }
        function get(key) {

            /* jshint validthis:true */
            var _this = this;

            if(!key) {
                return _this;
            } else if(this[key]) {
                return _this[key];
            } else {
                $log.error('Config: please pass existing property name');
                return _this;
            }

        }
        function set(key, value) {

            /* jshint validthis:true */
            var _this = this;

            if(key && value) {
                _this[key] = value;
                return _this[key];
            } else {
                $log.error('Config: please pass a key and value');
                return _this;
            }

        }

    }


})();
