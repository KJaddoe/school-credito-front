(function() { 'use strict';

    angular
        .module('site.auth')
        .controller('UserCtrl', UserCtrl);

    UserCtrl.$inject = ['$rootScope','User'];

    function UserCtrl($rootScope, User) {

        /* jshint validthis:true */
        var vm = this;

        var user = {};

        user.token = $rootScope.isAuthenticated;
        vm.user = User.getUserData(user);

    }

})();
