(function() { 'use strict';

    angular
        .module('site.auth')
        .directive('user', user);

    function user() {

        return {
            restrict: 'EA',
            replace: true,
            link: postLink,
            templateUrl: '/html/user.directive.html',
            controller: 'UserCtrl',
            controllerAs: 'vm',
        };

        function postLink(scope, element, attr) {

        }

    }

})();
