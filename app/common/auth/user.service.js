(function() { 'use strict';

    angular
        .module('site.auth')
        .factory('User', User);

    User.$inject = ['$q', '$timeout', '$resource', '$localStorage', '$mdToast','$state', 'config'];

    function User($q, $timeout, $resource, $localStorage, $mdToast, $state, config) {

        return {
            setCurrentUser: setCurrentUser,
            getCurrentUser: getCurrentUser,
            isAuthenticated: isAuthenticated,
            login: login,
            logout: logout,
            getUserData: getUserData,
        };

        function setCurrentUser(token) {
            $localStorage.currentUser = token;
        }
        function getCurrentUser() {
            return $localStorage.currentUser;
        }
        function isAuthenticated() {
            var currentUser = getCurrentUser();

            if(currentUser) {
                return $q.when(currentUser);
            } else {
                return $q.reject(false);
            }
        }
        function login(user) {
            resource().login({action: 'login', email:user.email, password:user.password}, function (response) {
                if(response.error){
                    return $mdToast.show({
                        hideDelay: 6000,
                        position: 'bottom right',
                        template: '<md-toast><span>' + response.error + '</span></md-toast>',
                    });
                } else {
                    setCurrentUser(response.token);
                    $state.go('dashboard');
                }
            });
        }
        function logout() {
            setCurrentUser(null);
            $state.go('front');
        }
        function getUserData(user) {
            return resource().getUserData({action: 'getUserData', token: user.token}, function (response) {
                if(response.error){
                    return $mdToast.show({
                        hideDelay: 6000,
                        position: 'bottom right',
                        template: '<md-toast><span>' + response.error + '</span></md-toast>',
                    });
                } else {
                    return response;
                }
            });
        }

        function resource() {

            return $resource(config.get('baseUrl') + '',
                {},
                {
                    login: {
                        method: 'POST',
                        params: {
                            email: '@email',
                            password: '@password',
                        },
                    },
                    getUserData: {
                        method: 'POST',
                        params: {
                            token: '@token',
                        }
                    },
                }
            );

        }

    }

})();
