(function() { 'use strict';

    angular
        .module('site.auth')
        .controller('LoginFormCtrl', LoginFormCtrl);

    LoginFormCtrl.$inject = ['User'];

    function LoginFormCtrl(User) {

        /* jshint validthis:true */
        var vm = this;

        vm.login = User.login;

    }

})();
