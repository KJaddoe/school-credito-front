(function() { 'use strict';

    angular
        .module('site.auth')
        .directive('loginForm', loginForm);

    function loginForm() {

        return {
            restrict: 'EA',
            replace: true,
            link: postLink,
            templateUrl: '/html/login.directive.html',
            controller: 'LoginFormCtrl',
            controllerAs: 'form',
        };

        function postLink(scope, element, attr) {

        }

    }

})();
