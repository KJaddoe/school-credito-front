(function() { 'use strict';

    angular
        .module('app.menu')
        .factory('MenuLinksService', getMenuLinks);

    getMenuLinks.$inject = ['$resource', 'config'];

    function getMenuLinks($resource, config) {

        return {
            home: {
                title: 'home',
                state: 'front',
            },
            organisaties: {
                title: 'Organisaties',
                state: 'front',
            },
            veiligBetalen: {
                title: 'Veilig betalen',
                state: 'front',
            },
            login: {
                title: 'Mijn Credito',
                state: 'login',
            },
        };

        /*return $resource(config.get('apiHost') + '/api/menu_items/:id?_format=:format',
            {},
            {
                getSiteMenu: {
                    method: 'GET',
                    params: {
                        id: 'site-menu',
                        format: 'hal_json',
                    },
                },
                getSocialMenu: {
                    method: 'GET',
                    params: {
                        id: 'social-menu',
                        format: 'hal_json',
                    },
                },
            }
        );*/
    }

})();
