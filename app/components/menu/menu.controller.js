(function() { 'use strict';

    angular
        .module('app.menu')
        .controller('menuCtrl', menuCtrl);

    menuCtrl.$inject = ['$scope', '$state', '$mdSidenav', 'MenuLinksService', 'User'];

    function menuCtrl($scope, $state, $mdSidenav, menu, User) {

        /* jshint validthis:true */
        var vm = this;

        $scope.logout = User.logout;

        vm.toggleMenu = buildToggler('left');

        vm.menu = menu;


        function buildToggler(componentId) {
            return function() {
                $mdSidenav(componentId).toggle();
            };
        }

    }

})();
