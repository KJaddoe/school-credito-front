(function() { 'use strict';

    angular.module('app.core', [

        /**
         * Third party libs
         */
        'ui.router',
        'ngStorage',
        'ngResource',
        'ngMaterial',
        'ngMdIcons',

        /**
         * site modules
         */
        'site.seo',
        'site.auth',

        /**
         * App modules
         */
        'app.menu',
        'app.footer',

    ])
    .run(run);

    run.$inject = ['$rootScope', '$state', 'seo', 'config'];

    function run($rootScope, $state, seo, config) {

        $rootScope.$state = $state;
        $rootScope.config = config;
        $rootScope.seo = seo;

        $rootScope.$on('$stateChangeError', onStateChangeError);

        function onStateChangeError(event, toState, toParams, fromState, fromParams, error) {

            if(error === false) {

                $state.go('login');

            }

        }

    }

})();
