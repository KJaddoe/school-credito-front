(function() { 'use strict';

    angular
        .module('app.core')
        .controller('CoreCtrl', CoreCtrl);

    CoreCtrl.$inject = ['$rootScope', '$scope', '$stateParams', '$locale', '$state', 'isAuthenticated', 'User'];

    function CoreCtrl($rootScope, $scope, $stateParams, $locale, $state, isAuthenticated, User) {

        $rootScope.isAuthenticated = isAuthenticated;

        if (User.isAuthenticated() && isAuthenticated === false) {
            $state.go('dashboard');
        }

        //$rootScope.lang = $stateParams.lang;

    }

})();
