(function() { 'use strict';

    angular
        .module('app.core')
        .config(config);

    config.$inject = ['$locationProvider', '$stateProvider', '$urlRouterProvider', '$windowProvider'];

    function config($locationProvider, $stateProvider, $urlRouterProvider, $windowProvider) {

        $locationProvider.html5Mode(true);
        $locationProvider.hashPrefix('!');
        $urlRouterProvider.otherwise('/');


        $stateProvider
            .state('auth', {
                url: '/',
                abstract: true,
                resolve: {
                    isAuthenticated: ['User', function(User) {
                        return User.isAuthenticated();
                    }],
                },
                views: {
                    page : {
                        templateUrl: '/html/page.html',
                        controller: 'CoreCtrl',
                    },
                },
            })
            .state('page', {
                url: '/',
                abstract: true,
                resolve: {
                    isAuthenticated: [function() {
                        return false;
                    }],
                },
                views: {
                    page : {
                        templateUrl: '/html/page.html',
                        controller: 'CoreCtrl',
                    },
                },
            })
            .state('about', {
                url: 'about',
                parent: 'page',
                resolve: {
                    seo: ['seo', function(seo) {
                        seo.set('pageTitle', 'About');

                        return seo;
                    }],
                },
                views: {
                    'content' : {
                        templateUrl: '/html/about.html',
                    },
                },
            })
            .state('dashboard', {
                url: 'user',
                parent: 'auth',
                resolve: {
                    seo: ['seo', function(seo) {
                        seo.set('pageTitle', 'User');

                        return seo;
                    }],
                },
                views: {
                    'content' : {
                        templateUrl: '/html/dashboard.html',
                    },
                },
            })
            .state('login', {
                url: 'login',
                parent: 'page',
                resolve: {
                    seo: ['seo', function(seo) {
                        seo.set('pageTitle', 'Login');

                        return seo;
                    }],
                },
                views: {
                    'content' : {
                        templateUrl: '/html/login.html',
                    },
                },
            })
            .state('front', {
                url: '',
                parent: 'page',
                resolve: {
                    seo: ['seo', function(seo) {
                        seo.set('pageTitle', 'Home');

                        return seo;
                    }],
                },
                views: {
                    'content' : {
                        templateUrl: '/html/front.html',
                    },
                },
            });
    }

})();
