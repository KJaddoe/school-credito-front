(function() { 'use strict';

    angular
        .module('app.core')
        .run(run);

    run.$inject = ['config'];

    function run(config) {

        config.init(function(config) {

            config.set('siteName', 'Credito-services');

            config.set('protocol', 'http://');
            config.set('apiHost', 'backend.credito.localhost.com/');

        });

    }

})();
